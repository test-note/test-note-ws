/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package note.service.autenticacion;

import note.service.administracion.dto.autenticacion.AutUsuarioOutDto;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 *
 * @author PCADMIN
 */
public interface AutenticacionService extends UserDetailsService {

    AutUsuarioOutDto obtenerUsuario(int idUsuario) throws Exception;

}
