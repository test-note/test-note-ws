/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package note.service.autenticacion.impl;

import com.querydsl.core.types.Predicate;
import java.util.HashSet;
import java.util.Set;
import note.logger.ExceptionUser;
import note.repository.TbUserRepository;
import note.repository.entity.QTbUser;
import note.repository.entity.TbUser;
import note.repository.entity.cuentas.UsuarioEntity;
import note.security.util.JwtUser;
import note.service.administracion.dto.autenticacion.AutUsuarioOutDto;
import note.service.autenticacion.AutenticacionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 *
 * @author PCADMIN
 */
@Service
public class AutenticacionServiceImpl implements AutenticacionService {

    @Autowired
    private TbUserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        try {
            UsuarioEntity usuarioEntity = userRepository.getUser(username);
            if (usuarioEntity == null) {
                throw new ExceptionUser("Credenciales incorrectas.");
            }
            Set<GrantedAuthority> grantedAuthorities = new HashSet();
            grantedAuthorities.add(new SimpleGrantedAuthority(usuarioEntity.getEmail()));
            return new JwtUser(username, usuarioEntity.getId(), usuarioEntity.getPassword(), grantedAuthorities);
        } catch (Exception ex) {
            throw new UsernameNotFoundException("Credenciales incorrectas.");
        }
    }

    @Override
    public AutUsuarioOutDto obtenerUsuario(int idUsuario) throws Exception {
        Predicate p = QTbUser.tbUser.id.eq(idUsuario);
        TbUser cuenta = userRepository.findOne(p).get();
        AutUsuarioOutDto usuarioOutDto = new AutUsuarioOutDto();
        usuarioOutDto.setEmail(cuenta.getEmail());
        usuarioOutDto.setIdUser(cuenta.getId());
        usuarioOutDto.setLastName(cuenta.getLastName());
        usuarioOutDto.setName(cuenta.getName());
        usuarioOutDto.setRole(cuenta.getRol());
        return usuarioOutDto;
    }

}
