/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package note.service.administracion.dto.autenticacion;

import lombok.Data;

/**
 *
 * @author mpilar
 */
@Data
public class AutLoginInDto {

    private String mail;
    private String password;
}
