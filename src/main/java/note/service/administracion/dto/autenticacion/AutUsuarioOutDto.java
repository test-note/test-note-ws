/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package note.service.administracion.dto.autenticacion;

import lombok.Data;

/**
 *
 * @author PCADMIN
 */
@Data
public class AutUsuarioOutDto {

    private String email;
    private String lastName;
    private String name;
    private String role;
    private int idUser;

}
