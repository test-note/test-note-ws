/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package note.service.administracion.dto.note;

import java.util.Date;
import java.util.List;
import note.repository.entity.TbNote;
import note.repository.entity.TbUser;
import org.mapstruct.BeforeMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

/**
 *
 * @author mpilar
 */
@Mapper(componentModel = "spring")
public interface NoteMapper {

    @BeforeMapping
    default void beforeMapping(@MappingTarget NoteMantDto target, TbNote source) {
        target.setIdUser(source.getUserId().getId());
        target.setNameUser(source.getUserId().getName() + " " + source.getUserId().getLastName());
    }

    NoteMantDto toNoteMantDto(TbNote note);

    List<NoteMantDto> toNoteMantDtoList(List<TbNote> note);

    @BeforeMapping
    default void beforeMapping(@MappingTarget TbNote target, NoteMantDto source) {
        if (source.getId() == null) {
            target.setRegisterDate(new Date());
            target.setUserId(new TbUser(source.getIdUser()));
        }
    }

    TbNote toNote(NoteMantDto note);

}
