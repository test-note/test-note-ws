/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package note.service.administracion.dto.note;

import javax.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

/**
 *
 * @author Martin Pilar
 */
@Data
public class NoteMantDto {

    @NotNull(message = "Debe ingresar el id del usuario")
    private Integer idUser;
    private Integer id;
    @NotNull(message = "La nota no puede ser null")
    @Length(min = 1, max = 205, message = "Debe ingresar al menos un caracter")
    private String note;
    private String nameUser;

}
