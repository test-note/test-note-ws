/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package note.service.administracion.dto.note;

import lombok.Data;

/**
 *
 * @author Martin Pilar
 */
@Data
public class NoteFilterDto {

    private int idUser;
    private String text;
    private int pageSize;
    private int pageIndex;
    private String sortCampo;
    private String sortOrden;
}
