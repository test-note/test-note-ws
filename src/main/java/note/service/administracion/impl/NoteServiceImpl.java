/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package note.service.administracion.impl;

import com.querydsl.core.types.Predicate;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import note.logger.ExceptionUser;
import note.repository.TbNoteRepository;
import note.repository.TbUserRepository;
import note.repository.entity.QTbNote;
import note.repository.entity.QTbUser;
import note.repository.entity.TbNote;
import note.repository.entity.TbUser;
import note.service.administracion.NoteService;
import note.service.administracion.dto.note.NoteFilterDto;
import note.service.administracion.dto.note.NoteMantDto;
import note.service.administracion.dto.note.NoteMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Martin Pilar
 */
@Service
public class NoteServiceImpl implements NoteService {

    @Autowired
    private TbNoteRepository noteRepository;
    @Autowired
    private TbUserRepository userRepository;
    @Autowired
    private NoteMapper noteMapper;

    @Override
    public PageImpl<NoteMantDto> getAll(NoteFilterDto in) throws Exception {
        GetValidateUser(in.getIdUser());
        PageRequest pageRequest = PageRequest.of(in.getPageIndex(), in.getPageSize());
        PageImpl<TbNote> pageEntity = noteRepository.listNotes(in.getSortCampo(), in.getSortOrden(), in.getText(), pageRequest);
        List<NoteMantDto> listaclienteOut = noteMapper.toNoteMantDtoList(pageEntity.toList());
        return new PageImpl<>(listaclienteOut, pageRequest, pageEntity.getTotalElements());
    }

    @Override
    public NoteMantDto get(int idUser, int id) throws Exception {
        GetValidateUser(idUser);
        Optional<TbNote> optNote = GetValidateNote(id, idUser);
        NoteMantDto mantenimientoDto = noteMapper.toNoteMantDto(optNote.get());
        return mantenimientoDto;
    }

    @Transactional
    @Override
    public NoteMantDto guardar(NoteMantDto mantenimientoDto) throws Exception {
        GetValidateUser(mantenimientoDto.getIdUser());
        TbNote note = noteMapper.toNote(mantenimientoDto);
        noteRepository.save(note);
        mantenimientoDto = noteMapper.toNoteMantDto(note);
        return mantenimientoDto;
    }

    @Transactional
    @Override
    public NoteMantDto actualizar(NoteMantDto mantenimientoDto) throws Exception {
        GetValidateUser(mantenimientoDto.getIdUser());
        Optional<TbNote> optNote = GetValidateNote(mantenimientoDto.getId(), mantenimientoDto.getIdUser());
        TbNote personaBD = optNote.get();
        personaBD.setNote(mantenimientoDto.getNote());
        personaBD.setUpdateDate(new Date());
        noteRepository.save(personaBD);
        return mantenimientoDto;
    }

    @Transactional
    @Override
    public void eliminar(int idUser, int id) throws Exception {
        GetValidateUser(idUser);
        Optional<TbNote> optNote = GetValidateNote(id, idUser);
        TbNote note = optNote.get();
        note.setUserId(new TbUser(idUser));
        noteRepository.delete(note);
    }

    private Optional<TbNote> GetValidateNote(Integer id, Integer idUser) throws ExceptionUser {
        if (id == null) {
            throw new ExceptionUser("La nota no existe", "La nota no existe" + id);
        }
        Predicate p = QTbNote.tbNote.userId.id.eq(idUser).and(QTbNote.tbNote.id.eq(id));
        Optional<TbNote> optNote = noteRepository.findOne(p);
        if (!optNote.isPresent()) {
            throw new ExceptionUser("La nota no existe");
        }
        return optNote;
    }

    private Optional<TbUser> GetValidateUser(Integer idUser) throws ExceptionUser {
        if (idUser == null) {
            throw new ExceptionUser("Usuario sin permisos para registrar", "La nota no existe" + idUser);
        }
        Predicate p = QTbUser.tbUser.id.eq(idUser);
        Optional<TbUser> optUser = userRepository.findOne(p);
        if (!optUser.isPresent()) {
            throw new ExceptionUser("El usuario no existe");
        }
        return optUser;
    }
}
