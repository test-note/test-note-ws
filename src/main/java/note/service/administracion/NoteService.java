/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package note.service.administracion;

import note.service.administracion.dto.note.NoteFilterDto;
import note.service.administracion.dto.note.NoteMantDto;

/**
 *
 * @author Martin Pilar
 */
public interface NoteService extends GenericService<NoteFilterDto, NoteMantDto, NoteMantDto> {

}
