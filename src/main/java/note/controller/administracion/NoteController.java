package note.controller.administracion;

import javax.validation.Valid;
import note.service.administracion.NoteService;
import note.service.administracion.dto.note.NoteFilterDto;
import note.service.administracion.dto.note.NoteMantDto;
import note.support.MensajeSupport;
import note.support.dto.Respuesta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Martin Pilar mpilarcastillejo@gmail.com
 */
@RestController
@RequestMapping("/api/notas")
public class NoteController {

    @Autowired
    private MensajeSupport mensajeSupport;

    @Autowired
    private NoteService noteService;

    @GetMapping("{idUser}/{idNote}")
    public Respuesta<NoteMantDto> get(@PathVariable int idUser, @PathVariable int idNote) throws Exception {
        NoteMantDto mantDto = noteService.get(idUser, idNote);
        return mensajeSupport.respuestaObtener(mantDto);
    }

    @PostMapping("listar")
    public Respuesta<Page<NoteMantDto>> getAll(@RequestBody NoteFilterDto inDto) throws Exception {
        Page<NoteMantDto> dato = noteService.getAll(inDto);
        return mensajeSupport.respuestaListar(dato);
    }

    @PostMapping()
    public Respuesta<NoteMantDto> guardar(@Valid @RequestBody NoteMantDto inDto) throws Exception {
        NoteMantDto mantOut = noteService.guardar(inDto);
        return mensajeSupport.respuestaAgregar(mantOut);
    }

    @PutMapping()
    public Respuesta<NoteMantDto> actualizar(@Valid @RequestBody NoteMantDto inDto) throws Exception {
        NoteMantDto dato = noteService.actualizar(inDto);
        return mensajeSupport.respuestaActualizar(dato);
    }

    @DeleteMapping("{idNote}/{idUser}")
    public Respuesta actualizarEstado(@PathVariable int idNote, @PathVariable int idUser) throws Exception {
        noteService.eliminar(idUser, idNote);
        return mensajeSupport.respuestaEliminar(null);
    }
}
