/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package note.controller.administracion;

import java.util.HashMap;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import note.security.util.JwtTokenUtil;
import note.security.util.JwtUser;
import note.service.administracion.dto.autenticacion.AutLoginInDto;
import note.service.administracion.dto.autenticacion.AutUsuarioOutDto;
import note.service.autenticacion.AutenticacionService;
import note.support.MensajeSupport;
import note.support.dto.Respuesta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author mpilar
 */
@RestController
@RequestMapping(value = "api/autenticacion")
@Slf4j
@CrossOrigin(methods = {RequestMethod.GET, RequestMethod.POST})
public class AutenticacionController {

    @Autowired
    private AutenticacionService autenticacionService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private MensajeSupport mensajeSupport;

    @PostMapping
    public ResponseEntity<Respuesta> Login(@RequestBody AutLoginInDto usuarioIn) throws Exception {
        String userName = usuarioIn.getMail().trim();
        UsernamePasswordAuthenticationToken upToken = new UsernamePasswordAuthenticationToken(userName, usuarioIn.getPassword());
        final Authentication authentication = authenticationManager.authenticate(upToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        final JwtUser userDetails = (JwtUser) authentication.getPrincipal();
        AutUsuarioOutDto autUsuarioOutDto = autenticacionService.obtenerUsuario(userDetails.getIdUser());
        Map<String, Object> claims = new HashMap<>();
        claims.put("user", autUsuarioOutDto);
        final String token = jwtTokenUtil.generateToken(claims, userDetails);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set(HttpHeaders.AUTHORIZATION, "Bearer " + token);
        httpHeaders.add(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS, HttpHeaders.AUTHORIZATION);
        return ResponseEntity.ok().headers(httpHeaders).body(mensajeSupport.respuestaObtener(autUsuarioOutDto));
    }
}
